import { PaymentIntentHandler } from './../src/paymentIntentHandler';


describe('paymentHandler test suite', () => {

  describe('handle()', () => {

    it('should not throw error when transferring correct payment', () => {
      // Arrange
      const dialogflowRequest = {
        parameters: {
          owner: 'menno',
          bankAccount: 'NL-000-001'
        }
      };

      const sut = new PaymentIntentHandler();

      // Assert
      expect(() => sut.handle(dialogflowRequest)).not.toThrowError()
    })

  })

  it('should throw error when owner is different from expected owner', () => {
      // Arrange
      const dialogflowRequest = {
        parameters: {
          owner: 'rik',
          bankAccount: 'NL-000-001'
        }
      };

      const sut = new PaymentIntentHandler();

      // Assert
      expect(() => sut.handle(dialogflowRequest)).toThrowError()
  })

})