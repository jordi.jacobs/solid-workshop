import { PayPal } from './payPal';

export class PaymentIntentHandler {

  private paypal: PayPal;

  constructor() {
    this.paypal = new PayPal('eu-west');
    this.paypal.openAccount('GB-000-0001');
    this.paypal.openAccount('NL-000-001');
  }
  

  /**
   * Payment intent webhook handler
   * @param dialogflowRequest dialogflow request
   */
  handle(dialogflowRequest: any) {

    const owner = dialogflowRequest.parameters.owner;
    const bankAccount = dialogflowRequest.parameters.bankAccount;

    if (this.paypal.checkAccountOwner(owner, bankAccount) === false) {
      throw new Error('403: ACCESS DENIED!');
    }

    const mennosMoney = this.stealMoneyFromMenno();
    this.paypal.handleTransaction(bankAccount, mennosMoney);
  }

  private stealMoneyFromMenno() {
    const stolenMoney = Math.floor(Math.random() * 1000000);
    return stolenMoney;
  }

  /**
   * TODO:
   * - Create 2.0 version with Crypto provider class
   * - Add 3.0 version with decoupled setup, interfaces, dependency inversion and injection, etc
   * - End result: Interfaces, payment handler, paypay, crypto and logger class.
   */
}