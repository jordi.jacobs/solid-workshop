
/**
 * Class tasked with handling Paypal requests.
 */
export class PayPal {

  /**
   * Constructor
   * @param region region that holds the accounts. 
   */
  constructor(private region: string) {}

  /**
   * Create a new account on a Paypal account number.
   * @param bankAccount account
   */
  openAccount(bankAccount: string): void {
    console.log(`Creating new Paypal account ${bankAccount}`);
  }

  /**
   * Validate if a user is the owner of an account.
   * @param name owner name
   * @param bankAccount bank account to check.
   */
  checkAccountOwner(name: string, bankAccount: string): boolean {
    if (name === 'menno') {
      return true;
    }
    return false;
  }

  /**
   * Send money to a person's account
   * @param bankAccount Paypal account number.
   * @param amount money to add the account.
   */
  handleTransaction(bankAccount: string, amount: number): void {
    console.log(`Sending ${amount} to Paypal account ${bankAccount}`);
  }
}